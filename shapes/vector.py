from __future__ import annotations
from math import sqrt, acos
from dataclasses import dataclass
from typing import Iterator


@dataclass(order=True)
class Vec3D:
    x: float = 0.0
    y: float = 0.0
    z: float = 0.0

    def __iter__(self: Vec3D) -> Iterator[float]:
        x = self.x
        y = self.y
        z = self.z
        yield x
        yield y
        yield z

    def __add__(self, other: Vec3D) -> Vec3D:
        return Vec3D(
            self.x + other.x,
            self.y + other.y,
            self.z + other.z
        )

    def __iadd__(self, other: Vec3D) -> None:
        self.x += other.x
        self.y += other.y
        self.z += other.z

    def __sub__(self: Vec3D, other: Vec3D) -> Vec3D:
        return Vec3D(
            self.x - other.x,
            self.y - other.y,
            self.z - other.z
        )

    def __isub__(self: Vec3D, other: Vec3D) -> None:
        self.x -= other.x
        self.y -= other.y
        self.z -= other.z

    def __mul__(self: Vec3D, other: float) -> Vec3D:
        return Vec3D(
            self.x*other,
            self.y*other,
            self.z*other
        )

    def __imul__(self: Vec3D, other: float) -> None:
        self.x *= other
        self.y *= other
        self.z *= other

    def dot(self: Vec3D, other: Vec3D) -> float:
        return self.x*other.x + self.y*other.y + self.z*other.z

    def magnitude(self: Vec3D) -> float:
        return sqrt(self.x**2 + self.y**2 + self.z**2)

    def direction(self: Vec3D) -> Vec3D:
        x = self.x
        y = self.y
        z = self.z
        mag = sqrt(x * x + y * y + z * z)
        return Vec3D(x / mag, y / mag, z / mag)

    def distance(self: Vec3D, other: Vec3D) -> float:
        return sqrt(
            (self.x - other.x)**2 +
            (self.y - other.y)**2 +
            (self.z - other.z)**2
        )

    def angle(self: Vec3D, other: Vec3D) -> float:
        x = self.x
        y = self.y
        z = self.z

        a = other.x
        b = other.y
        c = other.z

        dot = x*a + y*b + z*c
        mag1 = sqrt(x*x + y*y + z*z)
        mag2 = sqrt(a*a + b*b + c*c)
        return acos(dot/mag1/mag2)


@dataclass(order=True)
class Vec2D:
    x: float = 0.0
    y: float = 0.0

    def __iter__(self: Vec2D) -> Iterator[float]:
        x = self.x
        y = self.y
        yield x
        yield y

    def __add__(self, other: Vec2D) -> Vec2D:
        return Vec2D(
            self.x + other.x,
            self.y + other.y
        )

    def __iadd__(self, other: Vec2D) -> None:
        self.x += other.x
        self.y += other.y

    def __sub__(self: Vec2D, other: Vec2D) -> Vec2D:
        return Vec2D(
            self.x - other.x,
            self.y - other.y
        )

    def __isub__(self: Vec2D, other: Vec2D) -> None:
        self.x -= other.x
        self.y -= other.y

    def __mul__(self: Vec2D, other: float) -> Vec2D:
        return Vec2D(
            self.x*other,
            self.y*other
        )

    def __imul__(self: Vec2D, other: float) -> None:
        self.x *= other
        self.y *= other

    def dot(self: Vec2D, other: Vec2D) -> float:
        return self.x * other.x + self.y * other.y

    def magnitude(self: Vec2D) -> float:
        return sqrt(self.x ** 2 + self.y ** 2)

    def direction(self: Vec2D) -> Vec2D:
        x = self.x
        y = self.y
        mag = sqrt(x*x + y*y)
        return Vec2D(x/mag, y/mag)

    def distance(self: Vec2D, other: Vec2D) -> float:
        return sqrt(
            (self.x - other.x)**2 +
            (self.y - other.y)**2
        )

    def angle(self: Vec2D, other: Vec2D) -> float:
        x = self.x
        y = self.y

        a = other.x
        b = other.y

        dot = x*a + y*b
        mag1 = sqrt(x*x + y*y)
        mag2 = sqrt(a*a + b*b)
        return acos(dot/mag1/mag2)
