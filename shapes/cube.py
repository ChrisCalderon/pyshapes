from __future__ import annotations
from shapes.polymesh import PolygonMesh
from shapes.vector import Vec3D


class Cube(PolygonMesh):
    """A cube mesh."""
    def __init__(
            self: Cube,
            corner: Vec3D,
            side: float,
            random_colors: bool = True) -> None:
        """Create a cube by specifying a corner and the side length.

        The corner is treated as the top, right, front corner. The resulting
        cube is oriented so that it's faces are parallel to the xy, xz, and yz
        planes.

        If random_colors is True, then random colors are generated for each
        face.
        """
        delta_x = Vec3D(side, 0.0, 0.0)
        delta_y = Vec3D(0.0, side, 0.0)
        delta_z = Vec3D(0.0, 0.0, side)
        vertexes = [
            # top vertexes
            corner,
            corner - delta_x,
            corner - delta_x - delta_y,
            corner - delta_y,
            # bottom vertexes
            corner - delta_z,
            corner - delta_z - delta_x,
            corner - delta_z - delta_x - delta_y,
            corner - delta_z - delta_y
        ]
        faces = [
            [0, 1, 2, 3],  # top face
            [4, 5, 6, 7],  # bottom face
            [0, 1, 5, 4],  # right face
            [2, 3, 7, 6],  # left face
            [0, 3, 7, 4],  # front face
            [1, 2, 6, 5]   # rear face
        ]
        super().__init__(vertexes, faces, random_colors)
