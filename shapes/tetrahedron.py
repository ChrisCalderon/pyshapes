from __future__ import annotations
from math import sqrt
from shapes.polymesh import PolygonMesh
from shapes.vector import Vec3D

RAD6 = sqrt(6)
RAD3 = sqrt(3)


class Tetrahedron(PolygonMesh):
    """A tetrahedron mesh."""
    def __init__(
            self: Tetrahedron,
            top: Vec3D,
            side: float,
            random_colors: bool = True) -> None:
        """Create a regular tetrahedron from the top vertex and side length.

        The resulting tetrahedron is oriented with the bottom face parallel to
        the xy plane, and one face facing the positive x direction.

        If random_colors is True, then random colors are generated for each
        face.
        """
        delta_x1 = Vec3D(side*RAD3/6, 0.0, 0.0)
        delta_x2 = Vec3D(side*RAD3/3, 0.0, 0.0)
        delta_y = Vec3D(0.0, side/2, 0.0)
        delta_z = Vec3D(0.0, 0.0, side*RAD6/3)

        vertexes = [
            top,
            top - delta_z + delta_x1 + delta_y,
            top - delta_z + delta_x1 - delta_y,
            top - delta_z - delta_x2
        ]

        faces = [
            [0, 1, 2],
            [0, 3, 1],
            [0, 2, 3],
            [1, 2, 3]
        ]

        super().__init__(vertexes, faces, random_colors)
