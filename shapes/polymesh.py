from __future__ import annotations
from shapes.vector import Vec3D, Vec2D
from typing import List, Tuple
from math import sin, cos
from random import getrandbits


def centroid2d(vertexes: List[Vec3D], xy: bool = True) -> Vec2D:
    """Computes a 2D projection of the centroid of the polygon.

    The polygon is the list of vertexes. When the xy flag is True, this function
    returns the xy projection of the centroid. When the xy flag is False,
    this function returns the xz projection of the centroid.
    """
    c1 = 0.0
    c2 = 0.0
    signed_area = 0.0
    # signed_area is actually double the signed area,
    # but this is made up for by dividing the c terms
    # by 3 instead of 6.

    for i in range(len(vertexes) - 1):
        v1 = vertexes[i]
        v2 = vertexes[i+1]

        if xy:
            term = v1.x*v2.y - v2.x*v1.y
            c2 += (v1.y + v2.y)*term
        else:
            term = v1.x*v2.z - v2.x*v1.z
            c2 += (v1.z + v2.z)*term

        signed_area += term
        c1 += (v1.x + v2.x)*term

    return Vec2D(c1/signed_area/3, c2/signed_area/3)


class PolygonMesh:
    """A class for representing 3D shapes as a polygon mesh.

    A polygon mesh is a collection of vertexes, and faces constructed from
    those vertexes.

    The x-axis goes through the screen, with the positive end in-front of the
    screen and the negative end behind. The y-axis is horizontal, with the
    negative end on the left side of the screen and the positive end on the
    right. The z-axis is vertical, with the negative end at the bottom of the
    screen and the positive end at the top.
    """

    def __init__(
            self: PolygonMesh,
            vertexes: List[Vec3D],
            faces: List[List[int]],
            random_colors: bool = True) -> None:
        """Create a Shape3D object with a list of vertexes and a list of faces.

        Each face is represented by a list of integers referring to the indexes
        of the vertexes used to construct it. The colors correspond to each
        face.

        If random_colors is True, then random colors are generated for each
        face.
        """
        self.vertexes = vertexes
        self.faces = faces
        self.colors = []

        for i in range(len(faces)):
            if random_colors:
                k = getrandbits(24)
                r = k >> 16
                g = (k >> 8) & 0xff
                b = k & 0xff
                self.colors.append(
                    '#{:0>2x}{:0>2x}{:0>2x}'.format(r, g, b)
                )
            else:
                self.colors.append('#ffffff')

    def set_color(
            self: PolygonMesh,
            face: int,
            rgb: Tuple[int, int, int]) -> None:
        """Set the color of a face with an RGB tuple."""
        if not (0 < face < len(self.faces)):
            raise IndexError('Face index out range: {}'.format(face))

        for color in rgb:
            if (color & 255) != color:
                raise ValueError('RGB value must be a positive 8-bit value')

        self.colors[face] = '#{:0>2x}{:0>2x}{:0>2x}'.format(*rgb)

    def rotate(
            self: PolygonMesh,
            angle: float,
            direction: Vec3D,
            point: Vec3D) -> None:
        """Rotates the vertexes around an arbitrary axis.

        The axis is defined by a point and a direction vector.
        """
        # http://inside.mines.edu/~gmurray/ArbitraryAxisRotation/
        a, b, c = point
        u, v, w = direction

        s = sin(angle)
        c0 = cos(angle)
        c1 = 1 - c0

        u2 = u*u
        v2 = v*v
        w2 = w*w

        x1 = c1*(a*(v2 + w2) - u*(b*v + c*w)) + s*(b*w - c*v)
        x2 = u*c1
        y1 = c1*(b*(w2 + u2) - v*(c*w + a*u)) + s*(c*u - a*w)
        y2 = v*c1
        z1 = c1*(c*(u2 + v2) - w*(a*u + b*v)) + s*(a*v - b*u)
        z2 = w*c1

        for vert in self.vertexes:
            dotp = vert.dot(direction)
            x, y, z = vert
            vert.x = x1 + x2*dotp + x*c0 + s*(v*z - w*y)
            vert.y = y1 + y2*dotp + y*c0 + s*(w*x - u*z)
            vert.z = z1 + z2*dotp + z*c0 + s*(u*y - v*x)

    def translate(
            self: PolygonMesh,
            direction: Vec3D,
            distance: float) -> None:
        """Translate every vertex a given distance in a given direction."""
        direction = direction.direction()
        for vertex in self.vertexes:
            vertex += direction*distance

    def apply_perspective(
            self: PolygonMesh,
            camera_pos: Vec3D,
            viewer_pos: Vec3D) -> List[Tuple[str, List[Vec2D]]]:
        """Applies a perspective projection of the mesh onto the y/z plane.

        Returns a list of faces, which are lists of Vec2D vertexes, in order
        starting with the furthest face from the camera.
        """
        # http://en.wikipedia.org/wiki/3D_projection#Perspective_projection
        projected = []
        a, b, c = viewer_pos
        d, e, f = camera_pos
        for vert in self.vertexes:
            x = vert.x - d
            y = vert.y - e
            z = vert.z - f
            projected.append(Vec2D(b - a*y/x, c - a*z/x))

        face_distances = []
        for i in range(len(self.faces)):
            c = self._centroid(i)
            face_distances.append((camera_pos.distance(c), i))
        face_distances.sort()

        faces = []
        for d, i in face_distances:
            face = []
            color = self.colors[i]
            for v_i in self.faces[i]:
                face.append(projected[v_i])
            faces.append((color, face))

        return faces

    def _centroid(self: PolygonMesh, face: int) -> Vec3D:
        """Computes and returns the centroid of the face."""
        face_vertexes = []
        for index in self.faces[face]:
            face_vertexes.append(self.vertexes[index])

        c_xy = centroid2d(face_vertexes)
        c_xz = centroid2d(face_vertexes, xy=False)
        return Vec3D(c_xy.x, c_xy.y, c_xz.y)
